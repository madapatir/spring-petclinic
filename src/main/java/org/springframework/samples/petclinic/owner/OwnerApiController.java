package org.springframework.samples.petclinic.owner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OwnerApiController {

	@Autowired
	OwnerApiRepository ownerApiRepository;

	@GetMapping("getOwnerByLastName/{lastName}")
	public List<OwnerApi> getOwnerByLastName(@PathVariable String lastName) {
		return ownerApiRepository.findOwnerByLastName(lastName);
	}

	@GetMapping("getOwnerById/{ownerId}")
	public List<OwnerApi> getOwnerById(@PathVariable int ownerId) {
		return ownerApiRepository.findOwnerById(ownerId);
	}

	@GetMapping("getAllOwners")
	public List<OwnerApi> getAllOwners() {
		return ownerApiRepository.findAll();
	}

}
