package org.springframework.samples.petclinic.owner;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface OwnerApiRepository extends JpaRepository<OwnerApi, Integer> {

	List<OwnerApi> findOwnerByLastName(String lastName);

	List<OwnerApi> findOwnerById(int ownerId);

}
