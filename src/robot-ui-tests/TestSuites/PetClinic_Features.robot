*** Settings ***
Documentation    Suite description
Library  SeleniumLibrary
Library  OperatingSystem

*** Variables ***
${chromedriver_path}  'C:/bin/chromedriver.exe'

*** Test Cases ***
Verify PetClinic Application Opens Successfully
    [Tags]    DEBUG
    Launch PetClinic Application
    Close PetClinic Application


Verify Find Owner Feature Works Successfully
    [Tags]    DEBUG
    Launch PetClinic Application
    Select Find Owners
    Search For Owner With LastName  Black
    Verify Owner Is In List Of Owners  Jeff Black
    Close PetClinic Application

Verify Whether A New Owner Can Be Added
    Launch PetClinic Application
    Select Find Owners
    Select Add Owner
    Enter New Owner Details And Select Add Owner
    Select Find Owners
    Search For Owner With LastName  Madapati
    Verify Owner Is In List Of Owners  Ravi Madapati
    Close PetClinic Application


*** Keywords ***
Launch PetClinic Application
    open browser  http://localhost:8080  Chrome
    maximize browser window
    wait until page contains element  xpath://h2[contains(text(),'Welcome')]  timeout=30

Close PetClinic Application
    close browser

Select Find Owners
    Set Selenium Speed  0.25s
    click element  xpath://span[contains(text(),'Find owners')]
    Set Selenium Speed  0s

Search For Owner With LastName
    [Arguments]  ${owner_lastname}
    Set Selenium Speed  0.25s
    click element  id=lastName
    input text  id=lastName  ${owner_last_name}
    click button  xpath://*[@id="search-owner-form"]/div[2]/div/button
    wait until page contains element  xpath://h2[.='Owner Information']  timeout=30
    Set Selenium Speed  0s

Select Add Owner
    Set Selenium Speed  0.25s
    wait until page contains element  xpath://a[.='Add Owner']  timeout=30
    click element  xpath://a[.='Add Owner']
    wait until page contains element  id=firstName  timeout=30
    Set Selenium Speed  0s

Enter New Owner Details And Select Add Owner
    Set Selenium Speed  0.25s
    click element  id=firstName
    input text  id=firstName  Ravi
    input text  id=lastName  Madapati
    input text  id=address  Roscam
    input text  id=city  Galway
    input text  id=telephone  12345678
    click button  xpath://button[@class='btn btn-default'][contains(.,'Add Owner')]
    Set Selenium Speed  0s

Verify Owner Is In List Of Owners
    [Arguments]  ${owner}
    element text should be  xpath://b[contains(.,'${owner}')]  ${owner}